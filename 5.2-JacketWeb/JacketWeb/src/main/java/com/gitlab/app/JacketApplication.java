package com.gitlab.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JacketApplication {

    public static void main(String[] args) {
        SpringApplication.run(JacketApplication.class, args);
    }
}
