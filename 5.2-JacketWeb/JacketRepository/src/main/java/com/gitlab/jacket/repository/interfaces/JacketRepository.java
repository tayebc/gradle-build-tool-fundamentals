package com.gitlab.jacket.repository.interfaces;

import com.gitlab.jacket.models.Entry;
import com.gitlab.repository.Repository;


/**
 */
public interface JacketRepository extends Repository<Entry> {

}
