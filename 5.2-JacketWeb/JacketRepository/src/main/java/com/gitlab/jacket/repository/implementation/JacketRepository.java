package com.gitlab.jacket.repository.implementation;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.gitlab.jacket.models.Entry;
import com.gitlab.repository.BaseRepository;

@Named
public class JacketRepository implements com.gitlab.jacket.repository.interfaces.JacketRepository {

    private BaseRepository<Entry> baseRepository;

    @Inject
    public JacketRepository(BaseRepository<Entry> baseRepository) {
    	this.baseRepository = baseRepository;
    	System.out.println("*********** JacketRepository - baseRepository: " + baseRepository);
    }
    

    @Override
    public List<Entry> Entities() {
    	baseRepository.Entities();
        return new ArrayList<Entry>();
    }

    @Override
    public Entry New() {
        return null;
    }

    @Override
    public void Add(Entry entity) {

    }

    @Override
    public void Create(Entry entity) {

    }

    @Override
    public void Delete(Entry entity) {

    }

    @Override
    public void Save() {

    }
}
