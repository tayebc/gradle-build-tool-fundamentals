package com.gitlab.jacket.services;

import com.gitlab.jacket.models.JacketEntry;

import java.util.List;

/**
 * Created by kevin on 03/07/2015.
 */
public interface JacketEntryService {

    List<JacketEntry> getAllEntries();
}
